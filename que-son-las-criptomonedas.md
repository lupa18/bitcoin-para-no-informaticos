# ¿Qué son las criptomonedas?

No va a ser sencillo entender **qué son las criptomonedas**, pero tampoco es sencillo entender **qué es el dinero:**

* ¿Cómo es posible que esos trozos de papel que llamamos dinero tengan valor?
* ¿Qué son esos números que aparecen en el banco o en una pantalla indicando que ese dinero que nos pertenece?&#x20;
* ¿Cómo es posible que compremos utilizando un pedazo de plástico que tiene un largo número en su frene?&#x20;

Todas preguntas que **parecen sencillas, pero no lo son**; sin embargo convivimos con este sistema monetario y lo utilizamos cotidianamente sin cuestionarnos estas cosas.

Es decir, estamos muy acostumbrados a utilizar ese papel o esos metales emitidos por los Estados y acordamos que tienen un cierto **valor** que utilizamos para intercambiar **bienes** (como un juego de sillas), **servicios** (como atenderse con un médico) o incluso como **forma de ahorro**. El uso de tarjetas de crédito y cuentas bancarias aún se vuelve más abstracto y menos comprensible.

En definitiva todo se basa en formas de almacenamiento e intercambio de valor que tomamos como válidas porque nos vienen pre establecidas y **confiamos** en ellas: confiamos en el Estado, en los bancos y en esos papeles llamados billetes.

## ¿Qué características tienen las monedas?

Para que un material u objeto pueda servir como moneda, como dinero, tiene ciertas propiedades que debe cumplir: debe ser un bien escaso, divisible, duradero, fungible (que sirva para el intercambio) y principalmente, se debe gozar de confianza.

Los papeles o metales que estamos acostumbrados a utilizar, cumplen estas características en la mayoría de los casos. Muchas veces los Estados abusan de su poder y emiten moneda, generando inflación y haciendo que esta pierda su valor. Otras veces simplemente se le quita el respaldo a una moneda o se pierde la confianza y esta deja de tener valor.

Las criptomonedas, también cumplen estas características, y muchas veces también corren riesgos similares. Pero hay algunas diferencias que las hacen **sustancialmente diferentes**. Veamos algunas de ellas.

En el caso de la **confianza,** se basa en algoritmos y programas informáticos, liderados por empresas o comunidades, en vez de un Estado y su banco central. En nuestro caso, la confianza se establece partiendo de **un software y de los algoritmos** que lo componen, así como de su equipo de desarrollo. En el caso de Bitcoin tiene una comunidad de cientos de programadores.

Para que esta confianza sea efectiva, son muy importantes la **formas de gobernanza** de la comunidad y de su equipo de desarrollo, así como la transparencia con la que estos realizan su trabajo. Por esto desde nuestra perspectiva es imprescindible que una criptomoneda sea **software libre**. Esto lo retomaremos en breve.

Este software, este programa informático es el que se encarga de llevar el listado de transacciones que se realizan en todo el sistema, es decir, se encarga de construir el libro contable que se conoce como **blockchain** (o cadena de bloques). En otras palabras:&#x20;

> El **blockhain** es un libro contable **descentralizado** en el que se registran las **transacciones** de forma **(semi)pública** y donde los saldos no están asociados a los usuarios, sino a las **direcciones** que ellos controlan.

Hecha esa definición principal, veamos algunos elementos a la hora de afrontar el problema de la **confianza** y en general la adopción y uso de este tipo de tecnologías.

![Imagen: “Bitcoin and coffee” por Aranami licenciada con CC BY 2.0](assets/bitcoin-cofee.jpg)

## El código abierto

Una de las fortalezas de ests sistemas de **confianza**, es que el **código fuente** de las criptomonedas suele estar abierto, y más aún suelen ser **software libre**. Es la forma más adecuada para garantizar la posibilidad de una **auditoria permanente** sobre el software y por lo tanto asegurarnos de que no realice acciones fraudulentas. ¿De qué otra forma podríamos asegurarnos de auditar un programa informático y saber si realmente hace lo que promete sin tener su "diagrama" completo de instrucciones?

No es una tarea sencilla, pero sin el software libre sería prácticamente imposible y en todo caso deberíamos depositar nuestra confianza nuevamente en terceros. Cuando el software es libre, siempre tendrás la posibilidad de descargar el código fuente, leerlo, asegurarte que no haya instrucciones maliciosas y utilizarlo. Muchas veces esta no es tarea de una persona cualquiera sino de un programador, o incluso de grupos de programadores y organizaciones que se dedican a buscar errores y posibles negligencias.

Para comprenderlo mejor, piensa en un **software como un edificio, y en sus planos como el código fuente**. Saber si existen o no puertas secretas, saber cómo se ensamblaron sus piezas, saber qué materiales se utilizaron y la calidad de un edificio es clave para poder confiar en él. A veces confiamos en inspectores municipales que hagan la tarea, pero lo mejor sería asegurarnos por nosotras mismas que los materiales son buenos. Con el software pasa algo similar solo que muy diferente. Los programas informáticos dominan toda nuestra cotidianeidad y no deberíamos dejar que nos vendan cajas negras imposibles de auditar. Por ello debemos exigir que los desarrollos de criptomonedas se realicen bajo paradigma del software libre. Después de todo, las criptomonedas serán algo muy importante en nuestras vidas.

Otro elemento importantes en la **confianza** hacia las criptomonedas es la **criptografía**, justamente de allí viene su nombre.

## ¿Pero qué es la criptografía?

Son **operaciones matemáticas** que codifican o tranforman algún tipo de información. Estas operaciones son las que están involucradas en el almacenamiento seguro de contraseñas o en el envío correos electrónicos de forma que un tercero no pueda leerlos. Otro ejemplo es la web: hoy en día la gran mayoría del tráfico web que pasa por Internet está protegido por criptografía, para evitar que gobiernos y empresas mafiosas espíen a los ciudadanos (el protocolo de conexión segura https).

Para el caso de las criptomonedas, la criptografía permite almacenar la **transacción** en el libro contable (el blockchain) de una forma "indeleble": no solamente queda codificada y descrita de forma **unívoca**, sino que se encadena con la anterior. Y la próxima con la actual. De tal modo se va construyendo una "cadena" criptográfica y de allí su nombre "blockhcain".

La otra parte importante del sistema es que este libro contable que es el blockchain, está almacenado **en cada una de las computadoras** que corren el software bitcoin, con lo que se vuelve prácticamente imposible que se pueda falsificar una transacción.

## Una red distribuida

Otro de los pilares en los que se basan las criptomonedas es en su red "distribuida", es decir, son nodos interconectados, **sin un "centro" o "servidor"**, sobre el cual se **distribuye y replica** el libro contable o blockchain.

Las topologías de red más conocidas son: centralizadas, descentralizadas o distribuidas (ver imagan abajo). En la primer topología, la **centralizada**, existe un "servidor" y un "cliente". Es decir, hay una jerarquía donde la información está en un nodo central o principal que es el servidor y luego los clientes o consumidores o nodos secundarios. Un ejemplo de esto es una página web, en donde hay un nodo central (el servidor) que provee la información (la página), y un cliente (la persona que accede desde su navegador). Si por algún motivo el servidor falla, la web queda inaccesible (se cae) y el cliente no puede acceder a la información.

Otro tipo de red es la **descentralizada**, donde hay varios nodos centrales que proveen la información y otros nodos que la consumen. Esta es una red más fuerte y democrática, pues para poder acabar con ella es necesario atacar a varios centros y la información no está en un solo nodo central. Las redes sociales libres como [diaspora\*](https://es.wikipedia.org/wiki/Diaspora\_\(red\_social\)) o [mastodon](https://es.wikipedia.org/wiki/Mastodon\_\(red\_social\)) son ejemplos de este tipo de red.

Por último la **distribuida** o también conocida como **peer-to-peer** (p2p, que significa "entre pares") en la que **todos los nodos son productores y consumidores**: todos los nodos tienen la información. Esto hace que le red sea mucho más fuerte y democrática y para acabar con ella hace falta atacar **todos los nodos** de la red. Es decir, la la caída o compromiso de uno o algunos nodos, no pone en peligro la **integridad de la red**. Un ejemplo de ello es la red [Zeronet](https://es.wikipedia.org/wiki/ZeroNet), que corre sobre Internet: una vez que se accede a un sitio web, este ya queda copiado en tu computadora y puede ser accedido por otros, así tu computadora se comporta como cliente y servidor. Otro ejemplo es la forma de intercambio de contenidos [BitTorrent](https://es.wikipedia.org/wiki/BitTorrent), por la cual te descargas archivos y al mismo tiempo estás compartiendo desde tu computadora.

![Imagen: topologías de red. Tomado de: https://lasindias.blog/el-poder-de-las-redes](assets/xtopologias\_de\_red.gif.pagespeed.ic.aa1tLCF4ND.png)

Las **criptomonedas**, funcionan construyendo una red **distribuida** de nodos a través de los cuales se copia y sincroniza el **blockchain**. Cuando instalamos una criptomoneda lo primero que ocurre es la descarga del **blockchain** y cada vez que la encendemos se sincroniza el resto de los nodos de la red. En cada momento del tiempo vamos teniendo todo el listado de transacciones que se van realizando.

En los momentos de escribir/revisar este artículo, hay unos 14.700 nodos completos de bitcoin en ejecución (2022).

## En resumen

Una criptomoneda es una forma de **creación de valor**, que utiliza un **registro de transacciones público** basado en **software libre**, mantenido y utilizado por una comunidad, en una **red distribuida**, en la que cada transacción se replica a toda la red utilizando **criptografía.**&#x20;

_¿Te gustó el artículo? ¿te parece de utilidad? Haz comentarios abajo así nos impulsas a seguir creando los próximos capítulos de la serie._&#x20;
